﻿Imports Microsoft.Office.Interop.Excel

Public Class Excel
    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Releases the com object described by obj. </summary>
    '''
    ''' <remarks>   A Chiarenza, 27/04/2020. </remarks>
    '''
    ''' <param name="obj">  The object. </param>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Sub ReleaseComObject(ByVal obj As Object)
        Try
            Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        End Try
    End Sub

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Is excel file. </summary>
    '''
    ''' <remarks>   A Chiarenza, 27/04/2020. </remarks>
    '''
    ''' <param name="file"> The file. </param>
    '''
    ''' <returns>   . </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function IsExcelFile(file As String)
        If IO.Path.GetExtension(file) = ".xlsx" Or IO.Path.GetExtension(file) = ".xls" Then
            IsExcelFile = True
        Else
            IsExcelFile = False
        End If
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Export excel. </summary>
    '''
    ''' <remarks>   A Chiarenza, 29/04/2020. </remarks>
    '''
    ''' <param name="excelfile">    The excelfile. </param>
    ''' <param name="dtgridExport"> The dtgrid export. </param>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Sub exportExcel(excelfile As String, dtgridExport As Data.DataTable)
        Dim xlApp As Application = New Microsoft.Office.Interop.Excel.Application()

        Dim xlWorkBook As Workbook
        xlWorkBook = xlApp.Workbooks.Add()
        Dim xlWorkSheet As Worksheet = xlWorkBook.Sheets(1)

        For Each column As Data.DataColumn In dtgridExport.Columns
            xlWorkSheet.Cells(1, column.Ordinal + 1).Value = column.ColumnName
        Next

        For Each row As Data.DataRow In dtgridExport.Rows
            For Each column As Data.DataColumn In dtgridExport.Columns
                If Not row(column.Ordinal).ToString = "" Then
                    xlWorkSheet.Cells(dtgridExport.Rows.IndexOf(row) + 1, column.Ordinal + 1).Value = row(column.Ordinal).ToString
                End If
            Next
        Next

        xlWorkBook.SaveAs(excelfile)
        xlWorkBook.Close()
        xlApp.Quit()

        ReleaseComObject(xlWorkSheet)
        ReleaseComObject(xlWorkBook)
        ReleaseComObject(xlApp)
    End Sub
End Class